// Copyright Epic Games, Inc. All Rights Reserved.

#include "TD_Skillbox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TD_Skillbox, "TD_Skillbox" );

DEFINE_LOG_CATEGORY(LogTD_Skillbox)
 