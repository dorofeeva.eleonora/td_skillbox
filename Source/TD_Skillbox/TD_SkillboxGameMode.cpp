// Copyright Epic Games, Inc. All Rights Reserved.

#include "TD_SkillboxGameMode.h"
#include "TD_SkillboxPlayerController.h"
#include "TD_SkillboxCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATD_SkillboxGameMode::ATD_SkillboxGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATD_SkillboxPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}